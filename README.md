[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)
[![CocoaPods Compatible](https://img.shields.io/cocoapods/v/EZSwiftExtensions.svg)](https://img.shields.io/cocoapods/v/LFAlertController.svg)  

# MyMonee
Apllication that will keep you on track when using your money

<p align="col">
![Screenshoot](/image/Screen_Shot_2021-05-31_at_17.45.39.png)
![Screenshoot](/image/Screen_Shot_2021-05-31_at_17.45.49.png)

</p>


## Features

- [x] Penggunaan
- [x] Impian
- [x] Profile


## Requirements

- iOS 11.0+
- Xcode 12.4

## Installation

#### Clone Git or Download Git

```Clone Git from your terminal
git clone https://gitlab.com/widiasamosir/mymonee.git
```

```Download Git from your terminal
1. Download zip from https://gitlab.com/widiasamosir/mymonee.git
2. Extract zip
```


#### CocoaPods
You can use [CocoaPods](http://cocoapods.org/) to install `Library` by adding it to your `Podfile`:

```Kingfisher
platform :ios, '11.0'
use_frameworks!
pod 'Kingfisher'
```



## Usage example
### Run App
1. Go to /MyMonee folder
2. Run code below
```
pod install
```
3. Open App > MyMonee.xcworkspace
4. Set your simulator and run your project.  
5. Congratulations!  

### Usage Details


##### 1. MainTab
MainTab berisikan file swift dan xib untuk mengatur tab bar yang terdapat pada aplikasi.

##### 2. Model

Model mengatur Entity yang akan digunakan dalam aplikasi. Contohnya: Pengeluaran, Customer, Impian

##### 3. Cell

Cell berisikan file swift dan xib dari TableViewCell custom yang akan digunakan pada view controller tableView

##### 4. Profile
Profile berisikan view controller untuk untuk menampilkan fitur profile dan mengatur alur navigasi profile

##### 5. Impian
Impian berisikan file/folder :
1. Service yang berisikan file service untuk GET, POST, DELETE, dan UPDATE data impian.
2. View controller impian untuk menampilkan fitur impian dan mengatur alur navigasi impian


#### 6. Main
Main merupakan scene utama dalam aplikasi yang tujuannya menampilkan penggunaan, berisikan file/folder :
1. Service yang berisikan file service untuk GET, POST, DELETE, dan UPDATE data penggunaan.
2. View controller penggunaan untuk menampilkan fitur penggunaan dan mengatur alur navigasi penggunaan

## Contribute

We would love you for the contribution to **MyMonee**.

## Meta

Widia Angelina Samosir  – widiasamosir@gmail.com
